package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T extends Comparable<T>> implements IQueue<T> 
{
	private SimpleNode<T> firstNode;
	private SimpleNode<T> lastNode;
	private int size;

	public Queue()
	{
		firstNode = null;
		lastNode = null;
		size = 0;	
		verifyInvariant();
	}
	
	public void enqueue(T item)
	{
		SimpleNode<T> tempLast = lastNode;
		lastNode = new SimpleNode<T>(item);
		lastNode.changeNext(null);
		if(isEmpty())
		{
			firstNode = lastNode;
		}
		else
		{
			tempLast.changeNext(lastNode);
		}
		size++;
		verifyInvariant();
	}
	
	@Override
	public T dequeue() 
	{
		if(firstNode == null)
		{
			return null;
		}
		
		T element = firstNode.getElement();
		firstNode = firstNode.getNextNode();
		size--;
		
		if(isEmpty())
		{
			lastNode = null;
		}
		verifyInvariant();

		return element;
	}

	@Override
	public boolean isEmpty()
	{
		return firstNode == null;
	}

	@Override
	public int size() 
	{
		return size;
	}
	
	public SimpleNode<T> getFirstNode()
	{
		return firstNode;
	}
	
	public SimpleNode<T> getLastNode()
	{
		return lastNode;
	}
	
	@Override
	public IteratorList<T> iterator() 
	{
		return new IteratorList<T>(firstNode);
	}

	public void verifyInvariant()
	{
		assert size >= 0: "El tamanio no puede ser menor a 0";
		if(size == 0)
		{
			assert firstNode == null: "Primer nodo debe ser null si no hay ningun elemento";
			assert lastNode == null: "Ultimo nodo debe ser null si no hay ningun elemento";

		}
		else if( size == 1)
		{
			assert firstNode != null : "Primer nodo deberia ser diferente de null";
			assert firstNode == lastNode : "El primer nodo deberia ser igual al ultimo";
			assert firstNode.getNextNode() == null :"El siguiente del primer nodo deberia ser null";
		}
		else
		{
			assert firstNode != null: "Primer nodo deberia ser diferente de null";
			assert firstNode != lastNode : "El primer nodo no deberia ser igual al ultimo";
			assert firstNode.getNextNode() != null : "El primer nodo deberia tener un siguiente";
			assert lastNode.getNextNode() == null : "El siguiente del ultimo nodo deberia ser null";
		}
	}
	
}
