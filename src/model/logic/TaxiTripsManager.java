package model.logic;

import java.io.FileReader;
import java.text.SimpleDateFormat;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import api.ITaxiTripsManager;
import model.data_structures.IteratorList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 

	public static final String TRIPSTTIME = "trip_start_timestamp";

	private Stack<Service> stackOfTripTime;
	private Queue<Service> queueOfTripTime;


	public TaxiTripsManager()
	{
		stackOfTripTime = new Stack<Service>();
		queueOfTripTime = new Queue<Service>();
	}
	public void loadServices(String serviceFile, String taxiId) 
	{
		SimpleDateFormat formateo = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss");
		JsonParser parser = new JsonParser();

		try 
		{
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				String company_name = "Independent";
				if(obj.get("company") != null)
				{ company_name = obj.get("company").getAsString(); }

				String taxi_id = "NaN";
				if ( obj.get("taxi_id") != null )
				{ taxi_id = obj.get("taxi_id").getAsString();}

				String trip_start_timestamp =  obj.get(TRIPSTTIME) != null ? obj.get(TRIPSTTIME).getAsString(): "nn";

				if(taxi_id.equals(taxiId))
				{

					stackOfTripTime.push(new Service(formateo.parse(trip_start_timestamp)));
					queueOfTripTime.enqueue(new Service(formateo.parse(trip_start_timestamp)));
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("El tama�o de la pila y cola son respectivamente:"+queueOfTripTime.size()+" y "+stackOfTripTime.size());

		// TODO Auto-generated method stub		
	}

	@Override
	public int [] servicesInInverseOrder()
	{
		int contDesOrdenados = 0;
		IteratorList<Service> iter = new IteratorList<Service>(stackOfTripTime.getActualNode());
		while(iter.hasNext())
		{
			System.out.println(iter.next().getTripStartTime());
		}
		int tamanioTot = stackOfTripTime.size();
		Service serv = stackOfTripTime.pop();
		Service serc = stackOfTripTime.pop();
		while(serc != null)
		{
			if(serv.getTripStartTime().compareTo(serc.getTripStartTime()) >= 0)
			{
				serv = serc;
				serc = stackOfTripTime.pop();
			}
			else
			{
				serc = stackOfTripTime.pop();
				contDesOrdenados++;
			}
		}
		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		resultado[0] = contDesOrdenados;
		resultado[1] = tamanioTot-contDesOrdenados;
		return resultado;
	}
	@Override
	public int [] servicesInOrder() 
	{
		int contDesOrdenados = 0;
		IteratorList<Service> iter = new IteratorList<Service>(queueOfTripTime.getFirstNode());
		while(iter.hasNext())
		{
			System.out.println(iter.next().getTripStartTime());
		}
		int tamanioTot = queueOfTripTime.size();
		Service serv = queueOfTripTime.dequeue();
		Service serc = queueOfTripTime.dequeue();
		while(serc != null)
		{
			if(serv.getTripStartTime().compareTo(serc.getTripStartTime()) <= 0)
			{
				serv = serc;
				serc = queueOfTripTime.dequeue();
			}
			else
			{
				serc = queueOfTripTime.dequeue();
				contDesOrdenados++;
			}
		}
		System.out.println("Inside servicesInOrder");
		int [] resultado = new int[2];
		resultado[0] = tamanioTot-contDesOrdenados;
		resultado[1] = contDesOrdenados;
		return resultado;
	}


}
