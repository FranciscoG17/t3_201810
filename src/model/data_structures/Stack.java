package model.data_structures;

import java.util.NoSuchElementException;

public class Stack<T extends Comparable<T>> implements IStack<T>  
{
	
	private SimpleNode<T> firstNodeOfTheStack;
	
	private int size;
		
	public Stack()
	{
		firstNodeOfTheStack = null;
		size = 0;
		verifyInvariant();
	}
	
	@Override
	public void push(T item)
	{
		SimpleNode<T> tempFirst = firstNodeOfTheStack;
		firstNodeOfTheStack = new SimpleNode<T>(item);
		firstNodeOfTheStack.changeNext(tempFirst);
		size++;
		verifyInvariant();
	}

	@Override
	public T pop() 
	{
//		if(isEmpty())
//		{
//			throw new NoSuchElementException("No se puede eliminar ya que no hay nada en la lista");
//		}
		if(firstNodeOfTheStack == null)
		{
			return null;
		}
		T element = firstNodeOfTheStack.getElement();
		firstNodeOfTheStack = firstNodeOfTheStack.getNextNode();
		size--;
		verifyInvariant();
		
		return element;
	}

	@Override
	public boolean isEmpty() 
	{
		return firstNodeOfTheStack == null;
	}
	
	public int size()
	{
		return size;
	}
	
	public SimpleNode<T> getActualNode()
	{
		return firstNodeOfTheStack;
	}

	@Override
	public IteratorList<T> iterator() 
	{
		return new IteratorList<T>(firstNodeOfTheStack);
	}
	
	public void verifyInvariant()
	{
		assert size >= 0: "El size de la pila no puede ser menor a 0";
		
		if(size == 0)
		{
			assert firstNodeOfTheStack == null: "El primer nodo tiene que ser null si el size es 0";
		}
		else if( size == 1)
		{
			assert firstNodeOfTheStack != null : "El primer nodo no puede ser nulo si el size > 0";
			assert firstNodeOfTheStack.getNextNode() == null : "Si solo existe el primer nodo su siguiente es nulo";
		}
		else
		{
			assert firstNodeOfTheStack != null : "Primer nodo no puede ser nulo";
			assert firstNodeOfTheStack.getNextNode() != null : "Siguiente del primer nodo no puede ser nulo";
		}
	}


}
